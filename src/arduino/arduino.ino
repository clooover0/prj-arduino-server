#define WHITE_LED 8
#define BLUE_LED 9
#define RED_LED 10

#define INPUT_ANALOG A0

void setup()
{
  Serial.begin(9600);
  pinMode(WHITE_LED, OUTPUT);
  pinMode(BLUE_LED, OUTPUT);
  pinMode(RED_LED, OUTPUT);
  digitalWrite(WHITE_LED, HIGH);
  delay(500);
  digitalWrite(BLUE_LED, HIGH);
  delay(500);
  digitalWrite(RED_LED, HIGH);
  delay(500);
}

void loop()
{
  if (analogRead(INPUT_ANALOG) > 60)
  {
    digitalWrite(RED_LED, HIGH);
  }
  else
  {
    digitalWrite(RED_LED, LOW);
  }
  if (analogRead(INPUT_ANALOG) > 20)
  {
    digitalWrite(WHITE_LED, HIGH);
  }
  else
  {
    digitalWrite(WHITE_LED, LOW);
  }
  if (analogRead(INPUT_ANALOG) > 45)
  {
    digitalWrite(BLUE_LED, HIGH);
  }
  else
  {
    digitalWrite(BLUE_LED, LOW);
  }

  Serial.println(analogRead(INPUT_ANALOG));
  delay(20);
}
