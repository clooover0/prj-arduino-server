const fs = require('fs');
const index = fs.readFileSync( './src/index.html');

const SerialPort = require('serialport');
const parsers = SerialPort.parsers;

const parser = new parsers.Readline({
    delimiter: '\r\n'
});

const portSerial = new SerialPort('/dev/cu.usbmodem1401',{
    baudRate: 9600,
    dataBits: 8,
    parity: 'none',
    stopBits: 1,
    flowControl: false
});

portSerial.pipe(parser);

const http = require('http'),
    WebSocketServer = require('ws').Server,
    port = 3000,
    host = 'localhost';

const server = http.createServer(function(req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end(index);
});

var wss = new WebSocketServer({
    server: server
});

// Message des clients
wss.on('connection', function (client) {
    client.on('message', function (message) {
        let msg = JSON.parse(message);
        console.log(msg)
    });
});

// Valeur envoyé par l'arduino
parser.on('data', function(data) {
    console.log(data)
    wss.clients.forEach(function each(client) {
        client.send(JSON.stringify({type: 'value', data: data}));
    });
});

server.listen(3000);